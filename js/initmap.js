    var bslist = [                            
    ['BCRV', 49.802000, 30.115200],
    ['BERT', 47.317666, 32.865944],
    ['BGS2', 49.540300, 30.870168],
    ['BRSP', 50.373203, 30.947019],
    ['CHR2', 48.269209, 25.962573],
    ['CHTK', 49.016958, 25.799450],
    ['DIZU', 48.529837, 35.076637],
    ['DNPR', 48.467480, 35.014633],
    ['FRAN', 48.914287, 24.714131],
    ['GENI', 46.169690, 34.807930],
    ['GIZV', 49.918610, 27.607220],
    ['GLPL', 47.672180, 36.237460],
    ['GRD2', 48.658600, 25.446600],
    ['IVNK', 50.932991, 29.903924],
    ['JTMR', 50.250700, 28.651400],
    ['KACH', 49.642500, 27.896200],
    ['KIRV', 48.517820, 32.229470],
    ['KOCH', 48.824200, 26.391940],
    ['KRRG', 47.935618, 33.399533],
    ['KUCH', 48.761940, 27.274170],
    ['KZLE', 50.913847, 31.116088],
    ['KZLS', 49.222706, 33.862019],
    ['LVNC', 48.408060, 26.608060],
    ['MKVC', 50.059232, 27.066045],
    ['NIKL', 46.948309, 32.049412],
    ['NMSH', 50.570000, 30.100000],
    ['NVSL', 46.824758, 35.737318],
    ['PART', 47.069700, 32.533900],
    ['PHM2', 50.057823, 31.489440],
    ['PLTV', 49.585590, 34.534930],
    ['PMSK', 49.399684, 36.223776],
    ['PTR4', 48.396472, 33.388226],
    ['PTRV', 48.296092, 33.317163],
    ['RJN2', 48.936171, 24.154682],
    ['RMNK', 48.495312, 27.250860],
    ['SHPT', 50.183330, 27.066670],
    ['SHSH', 49.874384, 34.017163],
    ['SMNV', 49.606297, 33.195437],
    ['TETV', 49.369591, 29.678579],
    ['VASL', 48.210000, 36.020000],
    ['VLCH', 49.548239, 26.225735],
    ['VRHV', 48.148431, 24.786217],
    ['VSLE', 47.024093, 34.934761],
    ['ZAKR', 49.838889, 26.848611],
    ['ZAST', 48.527311, 25.830122],
    ['ZPRG', 47.879619, 35.023729],

    ['ALX2', 48.670999, 33.112576],
    ['BRGN', 49.457397, 24.951201],
    ['CHRS', 51.515991, 31.348449],
    ['DVTS', 48.854589, 26.712599],
    ['GDRS', 50.598419, 29.446456],
    ['GLSV', 50.360000, 30.500000],
    ['HMTS', 49.454238, 26.920812],
    ['HRUB', 50.810434, 23.864114],
    ['HUST', 48.176158, 23.293948],
    ['IZRS', 45.333478, 28.835514],
    ['JARO', 50.017030, 22.667784],
    ['KHAR', 50.005100, 36.239002],
    ['KIRS', 50.439124, 30.430123],
    ['KPNS', 49.710130, 37.614283],
    ['MIZG', 48.527360, 23.503549],
    ['MUKA', 48.445796, 22.722383],
    ['MYKO', 49.523778, 23.979361],
    ['NARO', 50.350170, 23.337780],
    ['NEMO', 48.959439, 28.847911],
    ['POLN', 49.374165, 22.433571],
    ['RAHI', 48.053420, 24.201150],
    ['RDVL', 50.133300, 25.250000],
    ['SAMB', 49.519473, 23.200648],
    ['SANO', 49.559750, 22.200780],
    ['SKOL', 49.039997, 23.512592],
    ['SMLA', 49.201637, 31.866296],
    ['SOKA', 50.474864, 24.285601],
    ['SULP', 49.835586, 24.014483],
    ['TERN', 49.549771, 25.555750],
    ['TNT', 0.000000, 0.000000],
    ['VLVL', 50.830692, 24.350051],
    ['VNRS', 49.219677, 28.427294],
    ['WLOD', 51.544836, 23.557606],
    ['ZHAS', 49.248544, 30.102479],
    ['ZOLH', 49.799780, 24.903309],
    ['ZPRS', 47.828684, 35.161413]
  ];



var ib = new InfoBox({
    content: document.getElementById("wrapper"),
    isHidden: false,
     // position:marker.getPosition,
      pane: "floatPane",
      enableEventPropagation: false,
      disableAutoPan: false,
      maxWidth: 280,
      alignBottom: false,
      pixelOffset: new google.maps.Size(-140, 0),
      zIndex: null,
      boxClass: "infobox",
      closeBoxMargin: "8px",
      closeBoxURL:'images/ib-close.png',
      closeBoxTitle: "Закрити",    
      infoBoxClearance: new google.maps.Size(1, 1)
    });

    
function initMap() {
  var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 6,
      center: {lat: 48.517820, lng: 32.229470},
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions:{
        position: google.maps.ControlPosition.LEFT_BOTTOM
      }, 
      streetViewControl: false,
      fullscreenControl: false,
      scrollwheel: true, 
      zoomControl: true, 
      zoomControlOptions: { 
          position: google.maps.ControlPosition.RIGHT_CENTER 
      },
  });
  var ctaLayer1 = new google.maps.KmlLayer({       
      url: 'http://www.google.com/maps/d/kml?forcekml=1&mid=1hMEyGvR2-air0T-WnXn1xnq62asp2WVB&lid=FjDTV4l1UD8',
      map: map     
    });
  var ctaLayer2 = new google.maps.KmlLayer({       
      url: 'http://www.google.com/maps/d/kml?forcekml=1&mid=1hMEyGvR2-air0T-WnXn1xnq62asp2WVB&lid=9OYHBWVfvio',
      map: map     
    });

  google.maps.event.addListener(map, "click", function() { 
    ib.close() 
  });

  for (var i = 0; i < bslist.length; i++){
      var bs = bslist[i];
      setBS(map,bs);
  }; 

}

function setBS(map, bs){
    var bsLatLng = new google.maps.LatLng(bs[1], bs[2]);
    var image = {
        url: 'images/marker-icon.png',
        size: new google.maps.Size(30, 30),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 15),
        labelOrigin: new google.maps.Point(15, 38),
        //labelClass: "labels",
    };
    var marker = new google.maps.Marker({
        position: bsLatLng,
        map: map,
        icon: image,
        title: bs[0],
        //labelClass: "labels",
        label: {
            text: bs[0],
            //class: "labels", 
            fontWeight: 'bold',
            color: "#2E2E2E",
            //fontSize: 13px,
        }
    });


    google.maps.event.addListener(marker, "click", function (e) {
       // ib.setContent(document.getElementById('wrapper'));
        ib.open(map, marker);
    });
    
    return marker;



}


google.maps.event.addDomListener(window, 'load', initMap);



